﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using aaarrgh.Models;
using Xamarin.Forms;

namespace aaarrgh.ViewModels
{
    [QueryProperty(nameof(ItemId), nameof(ItemId))]
    public class ItemDetailViewModel : BaseViewModel
    {
        private string itemId;
        private string name;
        private int age;
        private string address;

        public string Id { get; set; }


        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }
        public int Age
        {
            get => age;
            set => SetProperty(ref age, value);
        }
        public string Address
        {
            get => address;
            set => SetProperty(ref address, value);
        }

        public string ItemId
        {
            get
            {
                return itemId;
            }
            set
            {
                itemId = value;
                LoadItemId(value);
            }
        }

        public async void LoadItemId(string itemId)
        {
            try
            {
                var item = await DataStore.GetItemAsync(itemId);
                Id = item.Id;
                Name = item.Name;
                Age = item.Age;
                Address = item.Address;
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load Item");
            }
        }
    }
}
