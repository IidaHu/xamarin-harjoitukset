﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using aaarrgh.Models;
using Xamarin.Forms;

namespace aaarrgh.ViewModels
{
    public class NewItemViewModel : BaseViewModel
    {
        private string name;
        private int age;
        private string address;

        public NewItemViewModel()
        {
            SaveCommand = new Command(OnSave, ValidateSave);
            CancelCommand = new Command(OnCancel);
            this.PropertyChanged +=
                (_, __) => SaveCommand.ChangeCanExecute();
        }

        private bool ValidateSave()
        {
            return !String.IsNullOrWhiteSpace(name)
                && !String.IsNullOrWhiteSpace(address)
                && name.Length >= 4
                && address.Length >= 4
                && age >= 10;
        }


        public string Name
        {
            get => name;
            set => SetProperty(ref name, value);
        }

        public int Age
        {
            get => age;
            set => SetProperty(ref age, value);
        }

        public string Address
        {
            get => address;
            set => SetProperty(ref address, value);
        }

        public Command SaveCommand { get; }
        public Command CancelCommand { get; }

        private async void OnCancel()
        {
            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }

        private async void OnSave()
        {
            Item newItem = new Item()
            {
                Id = Guid.NewGuid().ToString(),
                Name = Name,
                Address = Address,
                Age = Age
            };

            await DataStore.AddItemAsync(newItem);

            // This will pop the current page off the navigation stack
            await Shell.Current.GoToAsync("..");
        }
    }
}
