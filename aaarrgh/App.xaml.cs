﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using aaarrgh.Services;
using aaarrgh.Views;

namespace aaarrgh
{
    public partial class App : Application
    {

        public App()
        {
            InitializeComponent();

            DependencyService.Register<RealDataStore>();
            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
