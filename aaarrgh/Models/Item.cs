﻿using System;
using Newtonsoft.Json;

namespace aaarrgh.Models
{
    public class Item
    {
        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("nimi", NullValueHandling = NullValueHandling.Ignore)]
        public string Name { get; set; }

        [JsonProperty("osoite", NullValueHandling = NullValueHandling.Ignore)]
        public string Address { get; set; }

        private int _Age;
        [JsonProperty("ika", NullValueHandling = NullValueHandling.Ignore)]
        public int Age { 
            get {
                _Age = DateTime.Today.Year - DOB.Year;
                return _Age;
            } 
            set {
                _DOB = new DateTime(DateTime.Today.Year - value, DOB.Month, DOB.Day);
                _Age = value;
            } 
        }

        [JsonProperty("puhelin", NullValueHandling = NullValueHandling.Ignore)]
        public string Phone { get; set; }


        private DateTime _DOB;
        [JsonProperty("syntyma_aika", NullValueHandling = NullValueHandling.Ignore)]
        public DateTime DOB {
            get {
                return _DOB;
            }
            set {
                _DOB = value;
                _Age = DateTime.Today.Year - value.Year;
            } }
    }
}