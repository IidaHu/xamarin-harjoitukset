﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using aaarrgh.Models;

namespace aaarrgh.Services
{
    class RealDataStore : IDataStore<Item>
    {
        readonly List<Item> items;
        HttpClient client = new HttpClient();

        public RealDataStore()
        {
            items = new List<Item>()
            {
                new Item { Id = Guid.NewGuid().ToString(),
                    Name="Serppo",
                    Age=11,
                    DOB=DateTime.Parse("1992-05-09"), 
                    Address="apinakatu 2" }
            };

        }

        public async Task<bool> AddItemAsync(Item item)
        {
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> UpdateItemAsync(Item item)
        {
            var oldItem = items.Where((Item arg) => arg.Id == item.Id).FirstOrDefault();
            items.Remove(oldItem);
            items.Add(item);

            return await Task.FromResult(true);
        }

        public async Task<bool> DeleteItemAsync(string id)
        {
            var oldItem = items.Where((Item arg) => arg.Id == id).FirstOrDefault();
            items.Remove(oldItem);

            return await Task.FromResult(true);
        }

        public async Task<Item> GetItemAsync(string id)
        {
            return await Task.FromResult(items.FirstOrDefault(s => s.Id == id));
        }

        public async Task<IEnumerable<Item>> GetItemsAsync(bool forceRefresh = false)
        {
            if (!forceRefresh)
            {
                return await Task.FromResult(items);
            }

            var response = await client.GetStringAsync("https://codez.savonia.fi/jussi/api/json_data.php");
            var Items = JsonConvert.DeserializeObject<List<Item>>(response);
            items.Clear();
            foreach (var item in Items)
            {
                items.Add(item);
            }

            return await Task.FromResult(Items);
        }

    }

}
