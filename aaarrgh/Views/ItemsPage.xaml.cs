﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using aaarrgh.Models;
using aaarrgh.Views;
using aaarrgh.ViewModels;

namespace aaarrgh.Views
{
    public partial class ItemsPage : ContentPage
    {
        ItemsViewModel _viewModel;

        public ItemsPage()
        {
            InitializeComponent();

            BindingContext = _viewModel = new ItemsViewModel();

            EnablePullToRefresh(5000);

        }


        public async Task EnablePullToRefresh(int delay)
        {
            await Task.Delay(delay);
            System.Diagnostics.Debug.WriteLine("delay done, can refresh");
            refreshview.IsEnabled = true;
            _viewModel.IsRefreshing = false;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _viewModel.OnAppearing();
        }

       
    }
}