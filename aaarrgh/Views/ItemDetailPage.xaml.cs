﻿using System.ComponentModel;
using Xamarin.Forms;
using aaarrgh.ViewModels;

namespace aaarrgh.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
            BindingContext = new ItemDetailViewModel();
        }
    }
}