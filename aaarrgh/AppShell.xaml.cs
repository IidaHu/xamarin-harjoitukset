﻿using System;
using System.Collections.Generic;
using aaarrgh.ViewModels;
using aaarrgh.Views;
using Xamarin.Forms;

namespace aaarrgh
{
    public partial class AppShell : Xamarin.Forms.Shell
    {
        public AppShell()
        {
            InitializeComponent();
            Routing.RegisterRoute(nameof(ItemDetailPage), typeof(ItemDetailPage));
            Routing.RegisterRoute(nameof(NewItemPage), typeof(NewItemPage));
        }

    }
}
